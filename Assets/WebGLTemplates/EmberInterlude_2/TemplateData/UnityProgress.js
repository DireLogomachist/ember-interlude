function UnityProgress (dom) {
	this.progress = 0.0;
	this.message = "";
	this.dom = dom;

	var dotWidth = 18;
	var containerWidth = 70;

	var parent = dom.parentNode;

	var background = document.createElement("div");
	//background.style.background = "#4D4D4D";
	background.style.background = "black";
	background.style.position = "absolute";
	parent.appendChild(background);
	this.background = background;

	var messageArea = document.createElement("p");
	//messageArea.style.color = "white";
	messageArea.style.color = "#FEF9F0";
	messageArea.style.position = "absolute";
	messageArea.style.fontFamily = "Monaco,sans-serif"
	messageArea.style.letterSpacing = "5px"
	parent.appendChild(messageArea);
	this.messageArea = messageArea;

	//missing 8.5 pixels compared to image
	var dotContainer = document.createElement("div");
	dotContainer.className = "dotContainer"
	dotContainer.style.margin = "0 auto"
	parent.appendChild(dotContainer);
	this.dotContainer = dotContainer;

	var dot1 = document.createElement("div");
	dot1.className = "loadingDot";
	dot1.style.top = (0 - dotWidth/2) + 'px';
	dot1.style.left = dot1.style.top = (containerWidth/2 - dotWidth/2) + 'px';
	dotContainer.appendChild(dot1);
	this.dot1 = dot1;

	var dot2 = document.createElement("div");
	dot2.className = "loadingDot";
	dot2.style.top = (containerWidth/2 - dotWidth/2) + 'px';
	dot2.style.left = dot1.style.top = (containerWidth - dotWidth/2) + 'px';
	dotContainer.appendChild(dot2);
	this.dot2 = dot2;

	var dot3 = document.createElement("div");
	dot3.className = "loadingDot";
	dot3.style.top = (containerWidth - dotWidth/2) + 'px';
	dot3.style.left = dot1.style.top = (containerWidth/2 - dotWidth/2) + 'px';
	dotContainer.appendChild(dot3);
	this.dot3 = dot3;

	var dot4 = document.createElement("div");
	dot4.className = "loadingDot";
	dot4.style.top = (containerWidth/2 - dotWidth/2) + 'px';
	dot4.style.left = dot1.style.top = (0 - dotWidth/2) + 'px';
	dotContainer.appendChild(dot4);
	this.dot4 = dot4;

	/*
	var image = document.createElement("img");
	image.src = "TemplateData/loading.png"; 
	image.style.position = "absolute";
	parent.appendChild(image);
	this.image = image;
	*/

	this.SetProgress = function (progress) { 
		if (this.progress < progress)
			this.progress = progress; 
		this.messageArea.style.display = "inline";	
		//this.image.style.display = "inline";
		this.Update();
	}

	this.SetMessage = function (message) { 
		this.message = message; 
		this.background.style.display = "inline";
		this.Update();
	}

	this.Clear = function() {
		this.background.style.display = "none";
		//this.image.style.display = "none";
		this.messageArea.style.display = "none";
		this.dotContainer.style.display = "none";
	}

	this.Update = function() {
		this.background.style.top = this.dom.offsetTop + 'px';
		this.background.style.left = this.dom.offsetLeft + 'px';
		this.background.style.width = this.dom.offsetWidth + 'px';
		this.background.style.height = this.dom.offsetHeight + 'px';

		//var image = new Image();
		//image.src = this.image.src;
		/*
		this.image.style.top = this.dom.offsetTop + (this.dom.offsetHeight * 0.6 - image.height * 0.5) + 'px';
		this.image.style.left = this.dom.offsetLeft + (this.dom.offsetWidth * 0.5 - image.width * 0.5) + 'px';
		this.image.style.width = image.width + 'px';
		this.image.style.height = image.height + 'px';
		*/
		this.messageArea.style.top = this.dom.offsetTop + (this.dom.offsetHeight * 0.4) + 'px';
		this.messageArea.style.left = 0;
		this.messageArea.style.width = '100%';
		this.messageArea.style.textAlign = 'center';
		this.messageArea.innerHTML = "LOADING";

		this.dotContainer.style.top = this.dom.offsetTop + (this.dom.offsetHeight * 0.6 - 70 * 0.5) + 'px';
		this.dotContainer.style.left = this.dom.offsetLeft + (this.dom.offsetWidth * 0.5 - 70 * 0.5) + 'px';
	}

	this.Update ();
}
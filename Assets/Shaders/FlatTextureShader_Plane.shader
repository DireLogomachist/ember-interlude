﻿Shader "BasicShaders/FlatTextureShader_Plane" {
	Properties {
		_Color("Color Tint", Color) = (1.0,1.0,1.0,1.0)
		_MainTex("Diffuse Texture", 2D) = "white" {}
	}
	Subshader {
		Pass {
			Tags { "LightMode" = "ForwardBase" "Queue"="Transparent" "RenderType"="Transparent"}
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off //Only for single planes - messes with transparancy otherwise
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
		
			//user vars
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform fixed4 _Color;
			
			//structs
			struct vertexInput {
				half4 vertex : POSITION;
				half4 texCoord : TEXCOORD0;
			};
			struct vertexOutput {
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD0;
			};
			
			//Vertex function
			vertexOutput vert(vertexInput v) {
				vertexOutput o;
				
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texCoord;
				
				return o;
			}
			
			//Fragment function
			float4 frag(vertexOutput i) : COLOR {

				//texure mapping
				float4 tex = tex2D(_MainTex, i.tex.xy*_MainTex_ST.xy + _MainTex_ST.zw); //unwraps the texture, in effect
				
				return fixed4(tex.xyz*_Color.xyz,tex.a*_Color.a); //returns flat texture color (no lighting/shading)
			}
			ENDCG
		}
	}
	//Fallback "Specular"
}






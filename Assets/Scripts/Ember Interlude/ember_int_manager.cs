﻿using UnityEngine;
using System.Collections;


public class ember_int_manager : MonoBehaviour {
	bool volume = true;

	void Start () {}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Screen.fullScreen = false;
		}
		if(Input.GetKeyDown(KeyCode.F)) {
			Screen.fullScreen = true;
		}
		if(Input.GetKeyDown(KeyCode.M)) {
			volume = !volume;

			if(!volume) {
				//AudioListener.pause = true;
				AudioListener.volume = 0;
			} else {
				//AudioListener.pause = false;
				AudioListener.volume = 1;
			}
		}

	}

}

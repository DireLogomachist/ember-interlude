﻿using UnityEngine;
using System.Collections;

public class flame_controller : MonoBehaviour {

	public GameObject[] firstStage;
	public GameObject[] secondStage;

	public float delay = 2.0f;
	public float waitTime = 1.0f;

	void Start () {
		StartCoroutine(delayedTrigger());
	}

	IEnumerator delayedTrigger() {
		yield return new WaitForSeconds(delay);

		//trigger first stage particles
		foreach(GameObject system in firstStage) {
			system.GetComponent<ParticleSystem>().Play();
		}

		//schedule timed second batch
		StartCoroutine(timedTrigger());
	}

	IEnumerator timedTrigger() {
		yield return new WaitForSeconds(waitTime);
		foreach(GameObject system in secondStage) {
			system.GetComponent<ParticleSystem>().Play();
		}
	}
}

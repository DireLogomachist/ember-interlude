﻿using UnityEngine;
using System.Collections;

public class brace_behavior : MonoBehaviour {

	public float flashLength = 0.5f;

	Renderer rend;
	Color currentColor;

	[HideInInspector]
	public int numConnections = 0;

	void Start () {
		rend = gameObject.GetComponent<Renderer>();
		currentColor = rend.material.color;
	}

	void Update () {
		if(numConnections == 0) {
			gameObject.layer = LayerMask.NameToLayer("Default");
		}
	}

    //For projectile collisions
	void OnCollisionEnter(Collision c) {
		if(c.collider.tag == "Projectile") {
			Debug.Log("Spear hit");
			//Destroy (c.gameObject);
			StartCoroutine(colorChange());
		}
	}

    IEnumerator colorChange() {
		rend.material.color = Color.red;
		yield return new WaitForSeconds(flashLength);
		rend.material.color = currentColor;
	}
}

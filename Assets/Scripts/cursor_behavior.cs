﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class cursor_behavior : MonoBehaviour {
	private static cursor_behavior _instance;
    public static cursor_behavior Instance { get { return _instance; } }

	public bool showDefaultCursor = false;
	public bool placeAimMarker = false;
	public float raycastDistance = 100.0f;
	public GameObject markerPrefab;

	public void Awake() {
		//Singleton pattern
		if (_instance != null && _instance != this) {
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	void Start() {
		Cursor.visible = showDefaultCursor;
	}

	void Update() {
		gameObject.transform.position = Input.mousePosition;

		//on mouse down
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, raycastDistance)) {
				player_behavior.Instance.throwSpear(hit.point);
				if(placeAimMarker)
					placeMarker(hit.point);
			} else {
				Vector3 point = ray.origin + (ray.direction * raycastDistance);
				player_behavior.Instance.throwSpear(point);
				if(placeAimMarker)
					placeMarker(hit.point);
			}
		}
	}

	void placeMarker(Vector3 position) {
		GameObject marker = GameObject.Instantiate(markerPrefab);
		marker.transform.position = position;
	}
}

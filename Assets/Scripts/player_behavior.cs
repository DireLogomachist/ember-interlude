﻿using UnityEngine;
using System.Collections;

public class player_behavior : MonoBehaviour {
	private static player_behavior _instance;
    public static player_behavior Instance { get { return _instance; } }

    public GameObject spear_prefab;
    public GameObject spear_placeholder;
	public float spear_cooldown_time = 1;

	GameObject queued_spear;
	bool can_throw = true;


	public void Awake() {
		//Singleton pattern
		if (_instance != null && _instance != this) {
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}


	void Start() {
		//turn off renderer for placeholder
		//start drawing reticle

		loadSpear();
	}
	
	void Update() {
	}

	void loadSpear() {
		if(queued_spear == null || queued_spear.GetComponent<spear_behavior>().thrown == true) {
			queued_spear = GameObject.Instantiate(spear_prefab);
			queued_spear.transform.parent = gameObject.transform;
			queued_spear.transform.localPosition = spear_placeholder.transform.localPosition;
			queued_spear.transform.localRotation = spear_placeholder.transform.localRotation;
			//line up to cursor via raycast
		}
	}

	public void throwSpear(Vector3 target) { //add accuracy arg later...
		if(can_throw) {
			queued_spear.transform.LookAt(target, Vector3.up);
			queued_spear.GetComponent<spear_behavior>().fireSpear();
			StartCoroutine(spearCooldown());
		}
	}

	IEnumerator spearCooldown() {
		yield return new WaitForSeconds(spear_cooldown_time);
		loadSpear();
		can_throw = true;
	}
}

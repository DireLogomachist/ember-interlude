﻿using UnityEngine;
using System.Collections;

public class spin : MonoBehaviour {
	public float speed = 20.0f;
	
	public bool right = false;
	public bool up = false;
	public bool forward = false;

	void Start () {
	}
	
	void Update () {
		Vector3 rot = new Vector3(0,0,0);

		if(right) rot += Vector3.right;
		if(up) rot += Vector3.up;
		if(forward) rot += Vector3.forward;

        gameObject.transform.Rotate (rot, speed*Time.deltaTime*1.17f);
	}
}

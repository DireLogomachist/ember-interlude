﻿using UnityEngine;
using System.Collections;

public class structure_behavior : MonoBehaviour {

	public GameObject bracePrefab;
	public GameObject jointPrefab;
	public GameObject otherPrefab;

	public int width = 1;
	public int height = 1;
	public int depth = 1;

	public int braceSize = 8;
	public int jointSize = 4;

	GameObject[,,] joints;

	void Start() {
		generateStructure();
	}

	void Update() {
		//selfDestruct();
	}

	void generateStructure() {
		//delete child marker object
		if(gameObject.transform.GetChild(0) != null) {
			GameObject.Destroy(transform.GetChild(0).gameObject);
		}

		//create structure starting with joints array
		joints = new GameObject[width + 1, height, depth + 1];

		//x-axis is i
		//y-axis is j
		//z-axis is k

		for(int i = 0; i < width + 1; i++) {
			for(int j = 0; j < height; j++) {
				for(int k = 0; k < depth + 1; k++) {
					//create leg braces and connecting braces
					//link the braces and the nodes
					GameObject joint = GameObject.Instantiate(jointPrefab);
					joint.transform.parent = gameObject.transform;
					joint.transform.rotation = gameObject.transform.rotation;
					joint.transform.localPosition = new Vector3(braceSize * (i - 1), braceSize * j, braceSize * (k - 1));

					struct_joint joint_script = joint.GetComponent<struct_joint>();
					joint_script.setIndex(new Vector3(i,j,k));
					
					//each joint only ever creates braces in 3 directions, but will check their opposites to check for existing braces to connect to
					GameObject up_brace;
					GameObject right_brace;
					GameObject forward_brace;
					
					//create braces and add as configJoints
					if(i < width) {
						right_brace = GameObject.Instantiate(bracePrefab);
						right_brace.GetComponent<brace_behavior>().numConnections++;
						right_brace.transform.parent = gameObject.transform;
						right_brace.transform.localPosition = joint.transform.localPosition + new Vector3(braceSize * 1/2,0,0);
						right_brace.transform.Rotate(90.0f*Vector3.up, Space.World);
						//right_brace.transform.Rotate(90.0f*right_brace.transform.up);
						randomRotate(right_brace);
						joint_script.right.connectedBody = right_brace.GetComponent<Rigidbody>();
						enableConfigJoint(joint_script.right);
					}
					if(j < height-1) {
						up_brace = GameObject.Instantiate(bracePrefab);
						up_brace.GetComponent<brace_behavior>().numConnections++;
						up_brace.transform.parent = gameObject.transform;
						up_brace.transform.localPosition = joint.transform.localPosition + new Vector3(0, braceSize*1/2, 0);
						up_brace.transform.Rotate(90.0f*Vector3.right, Space.World);
						//up_brace.transform.Rotate(90.0f*up_brace.transform.right);
						randomRotate(up_brace);
						joint_script.up.connectedBody = up_brace.GetComponent<Rigidbody>();
						enableConfigJoint(joint_script.up);
					}
					if(k < depth) {
						forward_brace = GameObject.Instantiate(bracePrefab);
						forward_brace.GetComponent<brace_behavior>().numConnections++;
						forward_brace.transform.parent = gameObject.transform;
						forward_brace.transform.localPosition = joint.transform.localPosition + new Vector3(0,0,braceSize * 1/2);
						randomRotate(forward_brace);
						joint_script.forward.connectedBody = forward_brace.GetComponent<Rigidbody>();
						enableConfigJoint(joint_script.forward);
					}
					
					//check for existing braces on previous joints
					if(i > 0 && joints[i-1,j,k].GetComponent<struct_joint>().right != null) {//check for left brace
						joint_script.left.connectedBody = joints[i-1,j,k].GetComponent<struct_joint>().right.connectedBody.GetComponent<Rigidbody>();
						joints[i-1,j,k].GetComponent<struct_joint>().right.connectedBody.gameObject.GetComponent<brace_behavior>().numConnections++;
						enableConfigJoint(joint_script.left);
					}
					if(j > 0 && joints[i,j-1,k].GetComponent<struct_joint>().up != null) {//check for down brace
						joint_script.down.connectedBody = joints[i,j-1,k].GetComponent<struct_joint>().up.connectedBody.GetComponent<Rigidbody>();
						joints[i,j-1,k].GetComponent<struct_joint>().up.connectedBody.gameObject.GetComponent<brace_behavior>().numConnections++;
						enableConfigJoint(joint_script.down);
					}
					if(k > 0 && joints[i,j,k-1].GetComponent<struct_joint>().forward != null) {//check for back brace
						joint_script.back.connectedBody = joints[i,j,k-1].GetComponent<struct_joint>().forward.connectedBody.GetComponent<Rigidbody>();
						joints[i,j,k-1].GetComponent<struct_joint>().forward.connectedBody.gameObject.GetComponent<brace_behavior>().numConnections++;
						enableConfigJoint(joint_script.back);
					}

					//create legs on lowest level
					if(j == 0) {
						GameObject down_brace = GameObject.Instantiate(bracePrefab);
						down_brace.GetComponent<brace_behavior>().numConnections++;
						down_brace.transform.parent = gameObject.transform;
						down_brace.transform.localPosition = joint.transform.localPosition + new Vector3(0, -braceSize*1/2, 0);
						//down_brace.transform.Rotate(90.0f*Vector3.right, Space.World);
						down_brace.transform.Rotate(90.0f*down_brace.transform.right, Space.Self);
						randomRotate(down_brace);
						joint_script.down.connectedBody = down_brace.GetComponent<Rigidbody>();
						enableConfigJoint(joint_script.down);
					}

					//delete configJoints without a connectBody
					foreach(ConfigurableJoint configJoint in joint_script.allJoints) {
						if(configJoint.connectedBody == null) {
							Destroy(configJoint);
						}
					}

					joints[i,j,k] = joint;
				}
			}
		}
	}

	void enableConfigJoint(ConfigurableJoint configJoint) {
		configJoint.xMotion = ConfigurableJointMotion.Limited;
		configJoint.yMotion = ConfigurableJointMotion.Limited;
		configJoint.zMotion = ConfigurableJointMotion.Limited;

		configJoint.angularXMotion = ConfigurableJointMotion.Limited;
		configJoint.angularYMotion = ConfigurableJointMotion.Limited;
		configJoint.angularZMotion = ConfigurableJointMotion.Limited;
	}

	//OKAY HERE'S THE DEAL
	//worldspace rotation doesn't work if you rotate the structure parent
	//so move everything over to local
	//but uh don't use transform.up or anything stick with Vector3.up k?

	void randomRotate(GameObject brace) {
		int rand = Random.Range(0,4);
		int randFlip = Random.Range(0,2);
		Vector3 rot = (randFlip*180.0f)*brace.transform.right + rand*90.0f*brace.transform.forward;
		brace.transform.Rotate(rot, Space.World);
	}

	void selfDestruct() {
		//destroy the structure from the ground up
		//random schedule destruction starting from bottom row
		//poll rotation of joints - if enough average outside of vertical, then self destruct

		//currently just destroys everything at once
		for(int i = 0; i < width + 1; i++) {
			for(int j = 0; j < height; j++) {
				for(int k = 0; k < depth + 1; k++) {
					if(joints[i,j,k] != null)
						joints[i,j,k].GetComponent<struct_joint>().selfDestruct();
				}
			}
		}
	}
}

//need to test numConnections incrementing...
﻿using UnityEngine;
using System.Collections;

public class aim_path : MonoBehaviour { 

    public float drawSize = 1;
    public float drawDistance = 100;
    public float drawInterval = .15f;

    public GameObject drawObject;
    
	void Start () {
	    drawAimPath();
	}
	
	void Update () {
		
	}

    void drawAimPath() {
        for(int i = 0; i < Mathf.Ceil(drawDistance*drawInterval); i++) {
			if(i == 0) continue;

            GameObject pathMarker = GameObject.Instantiate(drawObject);
            pathMarker.transform.parent = gameObject.transform;
            pathMarker.transform.localPosition = new Vector3(0,0,0);
            pathMarker.transform.localPosition += pathMarker.transform.forward * i/drawInterval;
        }
    }
}
